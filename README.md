<h1  align="center"> Curso Typescript</h1>
<img src="./image/typescript.png" alt="">

## 💻 Sobre o projeto

<em>É uma aplicação web que permite que cadastremos uma data, uma quantidade, um valor de uma negociação e que possamos exibir no futuro em uma lista e que o usuário possa verificar o que foi cadastrado</em>

* ### Porque usar typescript
  * <p>Utilização de Generics</p>
  * <p>Um pouco sobre módulos do ECMASCRIPT</p>
  * <p>Modelagem de uma Negociação em Javascript</p>
  * <p>Buracos em nossa modelagem por limitações da linguagem Javascript</p>

* ### Typescript e compilador
  * <p>Download do TypeScript</p>
  * <p>Configuração do compilador e papel do tsconfig.json</p>
  * <p>Integração com scripts do Node.js</p>
  * <p>Modificadores de acesso private e public</p>
  * <p>Benefícios iniciais da linguagem TypeScript</p>

* ### Benefícios  da tipagem estática
  * <p>O tipo implícito any</p>
  * <p>Benefícios da tipagem estática</p>
  * <p>Mais configurações do compilador tsc</p>
  * <p>Retorno de método explícito</p>
  * <p>Conversão de valores da interface do usuário</p>

* ### Avanço na modelagem do domínio
  * <p>Modelagem da classe Negociacoes</p>
  * <p>Utilização de Generics</p>
  * <p>O tipo ReadonlyArray</p>


###  Rodando a aplicação web (Front End)

```bash
# Clone este repositório
$ git clone https://gitlab.com/TLAB-Processo-22.1/curso-alura-typescript.git
# Acesse a pasta do projeto no seu terminal/cmd
$ cd 
# Vá para a pasta da aplicação Front End
$ cd typescript-curso-1-arquivos-iniciais
# Instale as dependências
$ npm install
# Execute a aplicação em modo de desenvolvimento
$ npm run start
# A aplicação será aberta na porta:3000 - acesse http://localhost:3000
```

### Autor

| [<img src="https://gitlab.com/uploads/-/system/user/avatar/12118000/avatar.png?width=400" width=115><br><sub>Lucas Filipe</sub>](https://github.com/lucasFilppe) 
